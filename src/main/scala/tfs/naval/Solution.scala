package tfs.naval

object Solution {

  // определить, подходит ли корабль по своим характеристикам
  def validateShip(ship: Ship): Boolean = ???

  // определить, можно ли его поставить
  def validatePosition(ship: Ship, field: Field): Boolean = ???

  // добавить корабль во флот
  def enrichFleet(fleet: Fleet, name: String, ship: Ship): Fleet = ???

  def markUsedCells(field: Field, ship: Ship): Field = ???

  def tryAddShip(game: Game, name: String, ship: Ship): Game = ???
}
